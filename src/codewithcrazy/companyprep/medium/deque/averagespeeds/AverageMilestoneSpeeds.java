package codewithcrazy.companyprep.medium.deque.averagespeeds;

import java.util.*;

public class AverageMilestoneSpeeds {

    public static List<MilestoneSpeed> getAverageSpeeds(List<MilestoneSpeed> milestones){
        List<MilestoneSpeed> output = new ArrayList<>();
        milestones.sort(Comparator.comparing(MilestoneSpeed::getStart).thenComparing(MilestoneSpeed::getEnd));

        Deque<MilestoneSpeed> speedDeque = new ArrayDeque<>();
        for (MilestoneSpeed milestone : milestones) {
            speedDeque.offerLast(milestone);
        }

        int start, end, count = 1;
        double speed;
        MilestoneSpeed temp = speedDeque.poll();
        start = temp.getStart();
        end = temp.getEnd();
        speed = temp.getSpeed();
        while (!speedDeque.isEmpty()){
            temp = speedDeque.poll();
            if(temp.getStart() >= end){
                output.add(new MilestoneSpeed(start, end, speed/count));
                start = temp.getStart();
                end = temp.getEnd();
                speed = temp.getSpeed();
                count = 1;
            }
            else if(temp.getStart() != start){
                speedDeque.offerFirst(temp);
                speedDeque.offerFirst(new MilestoneSpeed(temp.getStart(), end, speed));
                end = temp.getStart();
            }
            else if(end == temp.getEnd()){
                speed +=  temp.getSpeed();
                count += 1;
            }
            else{
                speedDeque.offerFirst(new MilestoneSpeed(end, temp.getEnd(), temp.getSpeed()));
                speed += temp.getSpeed();
                count += 1;
            }
        }
        output.add(new MilestoneSpeed(start, end, speed/count));

        return output;
    }

    public static void main(String[] args) {
        List<MilestoneSpeed> speeds = new ArrayList<>();
        speeds.add(new MilestoneSpeed(2, 4, 40d));
        speeds.add(new MilestoneSpeed(1, 2, 30d));
        speeds.add(new MilestoneSpeed(7, 8, 60d));
        speeds.add(new MilestoneSpeed(3, 6, 50d));
        speeds.add(new MilestoneSpeed(3, 4, 70d));

        System.out.println("Input milestones: ");
        System.out.println(speeds);

        speeds = getAverageSpeeds(speeds);
        System.out.println("Average speeds: ");
        System.out.println(speeds);
    }

    static class MilestoneSpeed{
        Integer start;
        Integer end;
        Double speed;

        @Override
        public String toString() {
            return "MilestoneSpeed{" +
                    "start=" + start +
                    ", end=" + end +
                    ", speed=" + speed +
                    '}';
        }

        public MilestoneSpeed() {
        }

        public MilestoneSpeed(Integer start, Integer end, Double speed) {
            this.start = start;
            this.end = end;
            this.speed = speed;
        }

        public Integer getStart() {
            return start;
        }

        public Integer getEnd() {
            return end;
        }

        public Double getSpeed() {
            return speed;
        }
    }
}
