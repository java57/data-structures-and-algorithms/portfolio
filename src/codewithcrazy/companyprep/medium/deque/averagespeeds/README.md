**Problem**

```
Consider a list of milestones with speed in between them. Calculate average speed between all milestones.
If there are overlapping milestones, average speed should be there in output.
```

**Example**
```
Input:

|
|                70
|              |----|
|                     50
|              |--------------|
|              40                    60
|         |---------|              |----|
|      30
|    |----|
|
|
|____|____|____|____|____|____|____|____|____|____|
     1    2    3    4    5    6    7    8    9    10

Consider above diagram with milestones as:
Start   End     Speed
1       2       30
2       4       40
3       6       50
3       4       70
7       8       60


Output:
Start   End     Speed
1       2       30
2       3       40
3       4       53.33
4       6       50
7       8       60
```

**Explanation**
```
Since from miletone 3 to 4, there are three overlapping speeds, average speed for that milestone would be: (40+50+70)/3.
Others are self explanatory.
```