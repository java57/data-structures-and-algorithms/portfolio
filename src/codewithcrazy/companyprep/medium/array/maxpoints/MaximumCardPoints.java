package codewithcrazy.companyprep.medium.array.maxpoints;

import java.util.stream.Stream;

public class MaximumCardPoints {

    public int maxScore(int[] cardPoints, int k) {
        int end = cardPoints.length-1, sum = 0, maxScore;
        if(k==end+1){
            return sumArray(cardPoints, sum);
        }
        for(int i=0;i<k;i++){
            sum += cardPoints[i];
        }
        maxScore = sum;
        for(int i=cardPoints.length-1, j=0; j<k;j++,i--){
            sum = sum - cardPoints[k-j-1] + cardPoints[i];
            maxScore = Math.max(sum,maxScore);
        }
        return maxScore;
    }

    public int sumArray(int[] cardPoints, int sum){
        for (int cardPoint : cardPoints) {
            sum += cardPoint;
        }
        return sum;
    }

    public static void main(String[] args) {
        MaximumCardPoints maximumCardPoints = new MaximumCardPoints();
        System.out.println(maximumCardPoints.maxScore(Stream.of(1, 2, 3, 4, 5, 6, 1).mapToInt(a->a).toArray(), 3));
        System.out.println(maximumCardPoints.maxScore(Stream.of(2, 2, 2).mapToInt(a->a).toArray(), 2));
        System.out.println(maximumCardPoints.maxScore(Stream.of(9, 7, 7, 9, 7, 7, 9).mapToInt(a->a).toArray(), 7));
        System.out.println(maximumCardPoints.maxScore(Stream.of(1, 1000, 1).mapToInt(a->a).toArray(), 1));
    }
}
