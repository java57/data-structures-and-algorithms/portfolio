**Problem**

```
Given a 1-d array points and number of moves as k.
For each move you can either pick a point from front or end. Once a point is picked, next one to be considered.
Find max points you can have in k moves
```

**Example**
```
Input: [1, 2, 3, 4, 5, 6, 1] and k=3
Output: 12 [5 + 6 + 1]
Explanation: For first move it can be from either side.
             However in next move you will get max if you select 6.
```