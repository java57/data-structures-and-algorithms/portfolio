**Problem**

```
Consider a 2d plain and given two arrays of points and queries(with co-ordinate nad radius), calculate number of points inside the circle created by each query. 
```

**Example**
```
Input: points = [[1,3],[3,3],[5,3],[2,2]], queries = [[2,3,1],[4,3,1],[1,1,2]]
Output: [3,2,2]
Explanation: Query [2, 3, 1] will have 3 points in it, [1, 3], [2, 2] and [3, 3].
Other queries would work in similar manner.
```