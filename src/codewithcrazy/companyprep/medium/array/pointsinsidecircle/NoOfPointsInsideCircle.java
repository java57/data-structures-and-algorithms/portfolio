package codewithcrazy.companyprep.medium.array.pointsinsidecircle;

public class NoOfPointsInsideCircle {
    public static int[] countPoints(int[][] points, int[][] queries) {
        int[] output = new int[queries.length];
        int i = 0;
        for(int[] query : queries){
            int pointSum = 0;
            for(int[] point : points){
                if((point[0]-query[0])*(point[0]-query[0]) + (point[1]-query[1])*(point[1]-query[1]) <= query[2]*query[2]){
                    pointSum+=1;
                }
            }
            output[i++] = pointSum;
        }
        return output;
    }
}
