package codewithcrazy.companyprep.medium.array.minprodsum;

import java.util.Arrays;
import java.util.stream.Stream;

public class MinimizeProdSumOf2Arrays {
    public static int minProductSum(int[] nums1, int[] nums2){
        int n = nums1.length, sum = 0;
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        for(int i=0; i<n; i++){
            sum+=nums1[i]*nums2[n-1-i];
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(minProductSum(Stream.of(5, 3, 4, 2).mapToInt(i->i).toArray(),
                Stream.of(4, 2, 2, 5).mapToInt(i->i).toArray()));
    }
}
