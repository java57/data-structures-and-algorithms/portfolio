**Problem**

```
Given two 1-d arrays, find minimum product sum of them 
```

**Example**
```
Input: arr1 -> [4, 3, 2, 5], arr2 -> [2, 3, 1, 5]
Output: 2*5 + 3*3 + 4*2 + 5*1 = 32 
Explanation: Sort both arrays, take min of 1st array and multiply with max of 2nd. 
```