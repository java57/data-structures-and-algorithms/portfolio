**Problem**

```
Given an m*n matrix, sort it diagonally.
```

**Example**
```
Input:
3   3   1   1
2   2   1   2
1   1   1   2

Output: 
1   1   1   1
1   2   2   2
1   2   3   3

```