package codewithcrazy.companyprep.medium.array.sortmatrixdiagonal;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class SortMatrixDiagonally {

    public static int[][] diagonalSort(int[][] mat){
        Map<Integer, Queue<Integer>> sortedElements = new HashMap<>();
        for(int i=0; i<mat.length;i++){
            for(int j=0;j<mat[0].length;j++){
                if(sortedElements.containsKey(i-j)){
                    sortedElements.get(i-j).add(mat[i][j]);
                }
                else{
                    Queue<Integer> pq = new PriorityQueue<>();
                    pq.add(mat[i][j]);
                    sortedElements.put((i-j), pq);
                }
            }
        }
        for(int i=0; i<mat.length;i++){
            for(int j=0;j<mat[0].length;j++){
                mat[i][j] = sortedElements.get(i-j).poll();
            }
        }
        return mat;
    }


    public static void main(String[] args) {
        int[][] input = {{3, 3, 1, 1}, {2, 2, 1, 2}, {1, 1, 1, 2}};
        diagonalSort(input);
        for (int[] ints : input) {
            for (int j = 0; j < input[0].length; j++) {
                System.out.print(ints[j]+" ");
            }
            System.out.println();
        }
    }
}
