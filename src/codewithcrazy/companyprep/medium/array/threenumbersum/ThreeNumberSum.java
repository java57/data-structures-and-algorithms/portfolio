package codewithcrazy.companyprep.medium.array.threenumbersum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeNumberSum {

    public static List<Integer[]> threeNumberSum(int[] array, int targetSum) {
        List<Integer[]> output = new ArrayList<>();

        Arrays.sort(array);
        for(int i=0;i<array.length;i++){
            int j = i+1, k = array.length-1;
            while (j<k){
                int curr = array[i]+array[j]+array[k];
                if(curr == targetSum){
                    Integer[] tempOutput = {array[i], array[j], array[k]};
                    output.add(tempOutput);
                    j++;
                }
                else if(curr<targetSum){
                    j++;
                }
                else {
                    k--;
                }
            }
        }

        return output;
    }

    public static void main(String[] args) {
        int[] input = {12, 3, 1, 2, -6, 5, -8, 6};
        List<Integer[]> output = threeNumberSum(input, 0);
        output.forEach(o->{
            for(int i:o){
                System.out.print(i+" ");
            }
            System.out.println();
        });
    }
}
