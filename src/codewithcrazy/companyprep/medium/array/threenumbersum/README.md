**Problem**

```
Consider a 1-d array, find a list of triplets that sum up to target sum.
Solution should be sorted in ascending order. 
```

**Example**
```
Input:
arr: 12, 3, 1, 2, -6, 5, -8, 6
target sum: 0 

Output: 
[-8, 2, 6], [-8, 3, 5], [-6, 1, 5]

```