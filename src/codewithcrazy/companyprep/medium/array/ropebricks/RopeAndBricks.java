package codewithcrazy.companyprep.medium.array.ropebricks;

import java.util.PriorityQueue;
import java.util.Queue;

public class RopeAndBricks {
    public static int ropeAndBricks(int[] buildings, int bricks, int ropes){
        int i = 0, output = 0;
        Queue<Integer> bricksNeeded = new PriorityQueue<>();
        int jumpBricks = 0;
        while(i<buildings.length-1){
            if(buildings[i]<buildings[i+1]){
                if(ropes!=0){
                    bricksNeeded.add(buildings[i+1]-buildings[i]);
                    ropes-=1;
                }
                else{
                    jumpBricks = bricksNeeded.peek();
                    int neededBricks = buildings[i+1]-buildings[i];
                    if(neededBricks<jumpBricks && bricks>=neededBricks){
                        bricks-=buildings[i+1]-buildings[i];
                    }
                    else if(jumpBricks<=bricks){
                        bricksNeeded.poll();
                        bricksNeeded.add(buildings[i+1]-buildings[i]);
                    }
                    else{
                        output = i;
                        break;
                    }
                }
            }
            i+=1;
        }
        if(output == 0){
            return buildings.length-1;
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println(ropeAndBricks(new int[]{4, 2, 7, 6, 9, 11, 14, 12, 8}, 5, 2));
        System.out.println(ropeAndBricks(new int[]{4, 2, 7, 6, 9, 11, 14, 12, 8}, 5, 1));
    }
}
