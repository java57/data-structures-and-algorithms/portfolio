**Problem**

```
Given a 1-d array of height of buildings, number of bricks and ropes, provide building's index you can reach with given ropes and bricks.
While moving from building i to i+1 consider below:
1. If next building's height is less than current building, no need to use either bricks or ropes
2. If next building's height is greater than current building, use either bricks or ropes.
    2.1 Number of bricks would be determined by difference in two buildings.
    2.2 Otherwise 1 rope can be used to move 
```

**Example**
```
Input: 
buildings: [4, 2, 7, 6, 9, 11, 14, 12, 8], bricks: 5, ropes: 2

Output:
8

Explanation:
Use rope to move from index 1 to 2
Use 3 bricks to move from index 3 to 4
Use 2 bricks to move from index 4 to 5
Use rope to move from index 5 to 6
```
```
Input: 
buildings: [4, 2, 7, 6, 9, 11, 14, 12, 8], bricks: 5, ropes: 1

Output:
5

Explanation:
Use rope to move from index 1 to 2
Use 3 bricks to move from index 3 to 4
Use 2 bricks to move from index 4 to 5
```