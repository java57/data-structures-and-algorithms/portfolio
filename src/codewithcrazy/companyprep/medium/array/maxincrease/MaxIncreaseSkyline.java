package codewithcrazy.companyprep.medium.array.maxincrease;

public class MaxIncreaseSkyline {
    public static int maxIncreaseKeepingSkyline(int[][] grid) {
        int n = grid.length, outputSum = 0;
        int[][] output = new int[n][n];
        Coordinates temp;
        for(int i=0;i<n;i++){
            temp = getMaxFromRow(i, n, grid);
            output[temp.getI()][temp.getJ()] = grid[temp.getI()][temp.getJ()];
        }
        for(int i=0;i<n;i++){
            temp = getMaxFromCol(i, n, grid);
            output[temp.getI()][temp.getJ()] = grid[temp.getI()][temp.getJ()];
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(output[i][j]==0){
                    Coordinates maxRow = getMaxFromRow(i, n, output);
                    Coordinates maxCol = getMaxFromCol(j, n, output);
                    output[i][j] = Math.min(output[maxRow.getI()][maxRow.getJ()], output[maxCol.getI()][maxCol.getJ()]);
                }
            }
        }
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                outputSum+=output[i][j]-grid[i][j];
            }
        }
        return outputSum;
    }

    public static Coordinates getMaxFromRow(int row, int length, int[][] arr){
        int tempMax = Integer.MIN_VALUE;
        Coordinates maxValueCoordinate = new Coordinates(row, 0);
        for(int i=0;i<length;i++){
            if(arr[row][i]>tempMax){
                maxValueCoordinate.setJ(i);
                tempMax = arr[row][i];
            }
        }
        return maxValueCoordinate;
    }

    public static Coordinates getMaxFromCol(int col, int length, int[][] arr){
        int tempMax = Integer.MIN_VALUE;
        Coordinates maxValueCoordinate = new Coordinates(0, col);
        for(int i=0;i<length;i++){
            if(arr[i][col]>tempMax){
                maxValueCoordinate.setI(i);
                tempMax = arr[i][col];
            }
        }
        return maxValueCoordinate;
    }

    public static void main(String[] args) {
        int[][] input = {{3, 0, 8, 4}, {2, 4, 5, 7}, {9, 2, 6, 3}, {0, 3, 1, 0}};
        System.out.println(maxIncreaseKeepingSkyline(input));
    }

    static class Coordinates{
        private int i;
        private int j;

        public Coordinates(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public void setI(int i) {
            this.i = i;
        }

        public void setJ(int j) {
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }
}
