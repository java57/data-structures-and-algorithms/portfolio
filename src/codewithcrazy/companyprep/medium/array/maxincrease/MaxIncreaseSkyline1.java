package codewithcrazy.companyprep.medium.array.maxincrease;

public class MaxIncreaseSkyline1 {
    public static int maxIncreaseKeepingSkyline(int[][] grid) {
        int n = grid.length, outputSum = 0;
        int[] rowMax = new int[n];
        int[] colMax = new int[n];

        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                rowMax[i] = Math.max(rowMax[i], grid[i][j]);
                colMax[j] = Math.max(colMax[j], grid[i][j]);
            }
        }

        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                outputSum+=Math.min(rowMax[i], colMax[j])-grid[i][j];
            }
        }

        return outputSum;
    }

    public static void main(String[] args) {
        int[][] input = {{3, 0, 8, 4}, {2, 4, 5, 7}, {9, 2, 6, 3}, {0, 3, 1, 0}};
        System.out.println(maxIncreaseKeepingSkyline(input));
    }

}
