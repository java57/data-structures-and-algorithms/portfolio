package codewithcrazy.companyprep.medium.linkedlist.printreversell;

public class PrintReverseLinkedList2 {

    public void printLinkedListInReverse(ImmutableListNode2 head) {
        if(head == null){
            return;
        }
        printLinkedListInReverse(head.getNext());
        head.printValue();
    }

    public static void main(String[] args) {
        ImmutableListNode2 head = new ImmutableListNode2(1);
        head.next = new ImmutableListNode2(2);
        head.next.next = new ImmutableListNode2(3);
        head.next.next.next = new ImmutableListNode2(4);

        PrintReverseLinkedList2 reverseLinkedList = new PrintReverseLinkedList2();
        reverseLinkedList.printLinkedListInReverse(head);
    }

    static class ImmutableListNode2{
        private Integer data;
        private ImmutableListNode2 next;
        public ImmutableListNode2(Integer data){
            this.data = data;
            this.next = null;
        }
        public void printValue(){
            System.out.print(data+",");
        }
        public ImmutableListNode2 getNext(){
            return next;
        }
    }
}
