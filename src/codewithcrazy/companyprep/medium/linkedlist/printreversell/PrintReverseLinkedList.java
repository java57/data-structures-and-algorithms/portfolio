package codewithcrazy.companyprep.medium.linkedlist.printreversell;

import java.util.Stack;

public class PrintReverseLinkedList {

    public void printLinkedListInReverse(ImmutableListNode head) {
        Stack<ImmutableListNode> temp = new Stack<>();
        while(head != null){
            temp.push(head);
            head = head.next;
        }
        while(!temp.isEmpty()){
            ImmutableListNode test = temp.pop();
            test.printValue();
        }
    }

    public static void main(String[] args) {
        ImmutableListNode head = new ImmutableListNode(1);
        head.next = new ImmutableListNode(2);
        head.next.next = new ImmutableListNode(3);
        head.next.next.next = new ImmutableListNode(4);

        PrintReverseLinkedList reverseLinkedList = new PrintReverseLinkedList();
        reverseLinkedList.printLinkedListInReverse(head);
    }

    static class ImmutableListNode{
        private Integer data;
        private ImmutableListNode next;
        public ImmutableListNode(Integer data){
            this.data = data;
            this.next = null;
        }
        public void printValue(){
            System.out.print(data+",");
        }
        public ImmutableListNode getNext(){
            return next;
        }
    }
}
