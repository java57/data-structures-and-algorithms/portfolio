**Problem**

```
You've a singly linked list that you have to print in reverse order
```

**Example**
```
Input: head -> 1 -> 2 -> 3 -> 4 -> null
Output: 4 3 2 1
```

**Approaches**
```
1. First approach is to have a user defined stack. Read data from the linked list and push them to stack.
Once done, pop data from stack and print it.
2. Another approach is to use java heap using recursion. It would work in same manner as first one but can fail with high amount of data in input.
```