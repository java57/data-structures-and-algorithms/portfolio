package codewithcrazy.companyprep.medium.trees.diameter;

public class TreeDiameter {

    static int diameter;

    public static int binaryTreeDiameter(BinaryTree tree) {
        // Write your code here.
        diameter = 0;
        getHeight(tree);
        return diameter;
    }

    public static int getHeight(BinaryTree node){
        if(node == null){
            return 0;
        }
        int leftHeight = getHeight(node.left);
        int rightHeight = getHeight(node.right);
        if(leftHeight+rightHeight > diameter){
            diameter = leftHeight+rightHeight;
        }
        return Math.max(leftHeight, rightHeight)+1;
    }

    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        root.left = new BinaryTree(3);
        root.right = new BinaryTree(2);
        root.left.left = new BinaryTree(7);
        root.left.left.left = new BinaryTree(8);
        root.left.left.left.left = new BinaryTree(9);
        root.left.right = new BinaryTree(4);
        root.left.right.right = new BinaryTree(5);
        root.left.right.right.right = new BinaryTree(6);
        System.out.println(binaryTreeDiameter(root));

        root = new BinaryTree(1);
        root.left = new BinaryTree(2);
        root.right = new BinaryTree(3);
        root.left.left = new BinaryTree(4);
        root.left.right = new BinaryTree(5);
        root.right.left = new BinaryTree(6);
        root.right.right = new BinaryTree(7);
        System.out.println(binaryTreeDiameter(root));
    }

    static class BinaryTree {
        public int value;
        public BinaryTree left = null;
        public BinaryTree right = null;

        public BinaryTree(int value) {
            this.value = value;
        }
    }
}
