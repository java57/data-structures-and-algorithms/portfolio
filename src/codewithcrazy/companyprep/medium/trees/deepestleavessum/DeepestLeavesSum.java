package codewithcrazy.companyprep.medium.trees.deepestleavessum;

import java.util.*;

public class DeepestLeavesSum {

    public static int deepestLeavesSum(TreeNode root){
        Deque<TreeNode> nodes = new ArrayDeque<>();
        Deque<Integer> depth = new ArrayDeque<>();
        int currDepth = 0, maxDepth=0, depthSum = 0;
        nodes.offerLast(root);
        depth.offerLast(0);
        while (!nodes.isEmpty()){
            TreeNode node = nodes.poll();
            Integer nodeDepth = depth.poll();
            if(nodeDepth == maxDepth){
                depthSum+=node.val;
            }
            else if(nodeDepth > maxDepth){
                maxDepth = nodeDepth;
                currDepth += 1;
                depthSum = node.val;
            }
            if(node.left!=null){
                nodes.offerLast(node.left);
                depth.offerLast(currDepth+1);
            }
            if(node.right!=null){
                nodes.offerLast(node.right);
                depth.offerLast(currDepth+1);
            }
        }
        return depthSum;
    }

    public static void main(String[] args) {
        TreeNode head = new TreeNode(1);
        head.left = new TreeNode(2);
        head.right = new TreeNode(3);
        head.left.left = new TreeNode(4);
        head.left.right = new TreeNode(5);
        head.right.right = new TreeNode(6);
        head.left.left.left = new TreeNode(7);
        head.right.right.right = new TreeNode(8);
        System.out.println(deepestLeavesSum(head));
    }

    static class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
