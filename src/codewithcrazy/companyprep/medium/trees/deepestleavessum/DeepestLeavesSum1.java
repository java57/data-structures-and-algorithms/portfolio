package codewithcrazy.companyprep.medium.trees.deepestleavessum;

import java.util.ArrayDeque;
import java.util.Deque;

public class DeepestLeavesSum1 {

    static Integer deepestNode = 0, output = 0;

    public static int deepestLeavesSum(TreeNode root){
        deepestLeaveSum(root, 0);
        return output;
    }

    public static void deepestLeaveSum(TreeNode node, int depth){
        if(node == null){
            return;
        }
        if(depth == deepestNode){
            output+=node.val;
        }
        else if(depth > deepestNode){
            output = node.val;
            deepestNode = depth;
        }
        deepestLeaveSum(node.left, depth+1);
        deepestLeaveSum(node.right, depth+1);
    }

    public static void main(String[] args) {
        TreeNode head = new TreeNode(6);
        head.left = new TreeNode(7);
        head.right = new TreeNode(8);
        head.left.left = new TreeNode(2);
        head.left.right = new TreeNode(7);
        head.right.left = new TreeNode(1);
        head.right.right = new TreeNode(3);
        head.left.left.left = new TreeNode(9);
        head.left.right.left = new TreeNode(1);
        head.left.right.right = new TreeNode(4);
        head.right.right.right = new TreeNode(5);
        System.out.println(deepestLeavesSum(head));
    }

    static class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
