**Problem**

```
Given an array, create maximum binary tree using below logic.
1. Get max value from array and create a node
2. Use remaining left array for left sub tree
3. Use remaining right array for right sub tree
```

**Example**
```
Input: [3, 2, 1, 6, 0, 5] 
Output:             6
                  /   \
                 /     \
                3       5
                 \     /
                  2   0
                   \
                    1
```