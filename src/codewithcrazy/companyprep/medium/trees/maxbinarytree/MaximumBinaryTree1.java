package codewithcrazy.companyprep.medium.trees.maxbinarytree;

public class MaximumBinaryTree1 {

    public static TreeNode constructMaximumBinaryTree(int[] nums){
        return constructMaximumBinaryTree(nums, 0, nums.length);
    }

    public static TreeNode constructMaximumBinaryTree(int[] nums, int l, int r){
        if(l == r){
            return null;
        }
        int maxIdx = getMaxValueIdx(nums);
        TreeNode node = new TreeNode(nums[maxIdx]);
        node.left = constructMaximumBinaryTree(nums, 0, maxIdx-1);
        node.right = constructMaximumBinaryTree(nums, maxIdx+1, nums.length-1);
        return node;
    }

    public static int getMaxValueIdx(int[] array){
        int output=0;
        int temp = Integer.MIN_VALUE;
        for(int i=0;i<array.length;i++){
            if(array[i]> temp){
                output = i;
                temp = array[i];
            }
        }
        return output;
    }

    public static void main(String[] args) {

    }

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
