package codewithcrazy.companyprep.medium.trees.maxbinarytree;

public class MaximumBinaryTree {

    public static TreeNode constructMaximumBinaryTree(int[] nums){
        int maxIdx = getMaxValueIdx(nums);
        int[] newArr;
        TreeNode root = new TreeNode(nums[maxIdx]);;
        if(maxIdx == 0){
            newArr = new int[nums.length-1];
            System.arraycopy(nums, 1, newArr, 0, newArr.length);
            constructMaximumBinaryTree(newArr, root, "right");
        }
        else if(maxIdx == nums.length-1){
            newArr = new int[nums.length-1];
            System.arraycopy(nums, 0, newArr, 0, newArr.length);
            constructMaximumBinaryTree(newArr, root, "left");
        }
        else{
            newArr = new int[maxIdx];
            System.arraycopy(nums, 0, newArr, 0, newArr.length);
            constructMaximumBinaryTree(newArr, root, "left");
            newArr = new int[nums.length-1-maxIdx];
            System.arraycopy(nums, maxIdx+1, newArr, 0, newArr.length);
            constructMaximumBinaryTree(newArr, root, "right");
        }

        return root;
    }

    public static void constructMaximumBinaryTree(int[] remArray, TreeNode node, String identifier){
        int newMax = getMaxValueIdx(remArray);
        TreeNode temp;
        int[] newArr;
        if(identifier.equalsIgnoreCase("left")){
            node.left = new TreeNode(remArray[newMax]);
            temp = node.left;
        }
        else{
            node.right = new TreeNode(remArray[newMax]);
            temp = node.right;
        }
        if(remArray.length>1){
            if(newMax == 0){
                newArr = new int[remArray.length-1];
                System.arraycopy(remArray, 1, newArr, 0, remArray.length - 1);
                constructMaximumBinaryTree(newArr, temp, "right");
            }
            else if(newMax == remArray.length-1){
                newArr = new int[remArray.length-1];
                System.arraycopy(remArray, 0, newArr, 0, remArray.length-1);
                constructMaximumBinaryTree(newArr, temp, "left");
            }
            else{
                newArr = new int[newMax];
                System.arraycopy(remArray, 0, newArr, 0, newArr.length);
                constructMaximumBinaryTree(newArr, temp, "left");
                newArr = new int[remArray.length-1-newMax];
                System.arraycopy(remArray, newMax+1, newArr, 0, newArr.length);
                constructMaximumBinaryTree(newArr, temp, "right");
            }
        }

    }

    public static int getMaxValueIdx(int[] array){
        int output=0;
        int temp = Integer.MIN_VALUE;
        for(int i=0;i<array.length;i++){
            if(array[i]> temp){
                output = i;
                temp = array[i];
            }
        }
        return output;
    }

    public static void main(String[] args) {

    }

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
