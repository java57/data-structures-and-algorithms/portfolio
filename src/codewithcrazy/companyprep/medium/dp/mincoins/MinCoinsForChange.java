package codewithcrazy.companyprep.medium.dp.mincoins;

import java.util.Arrays;

public class MinCoinsForChange {
    public static int minNumberOfCoinsForChange(int n, int[] denoms) {
        // Write your code here.
        Arrays.sort(denoms);
        int[] coins = new int[n+1];
        coins[0] = 0;
        for(int i=1;i<n+1;i++){
            coins[i] = Integer.MAX_VALUE;
        }
        for(int den:denoms){
            for(int i=1; i<n+1;i++){
                if(i==den){
                    coins[i] = 1;
                }
                if(i>den && coins[i-den]!=Integer.MAX_VALUE){
                    coins[i] = Math.min(coins[den]+coins[i-den], coins[i]);
                }
            }
        }
        return coins[n]==Integer.MAX_VALUE?-1:coins[n];
    }

    public static void main(String[] args) {
        System.out.println(minNumberOfCoinsForChange(7, new int[]{1, 5, 10}));
    }
}
