**Problem**

```
Given an array of denominations and a target amount, calculate minimum coins required to achieve target amount 
```

**Example**
```
Input:
n: 7
denoms: [1, 5, 10]

Output:
3

```

**Explanation**
```
2*1 + 5*1 coins would be the minimum number of coins required to achieve 7
```