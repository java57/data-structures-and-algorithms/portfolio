**Problem**

```
Given two strings, calculate minimum number of edit operations required on first string to obtain second string.
Possible edit operations: insertion, deletion and substitution of a character.
```

**Example**
```
Input:
------
First string: abc
Second string: yabd

Output:
-------
2
```

**Explanation**
```
2 operations are required, addition of character y and replace c with d
```