package codewithcrazy.companyprep.medium.dp.levenshtein;

public class LevenshteinDistance {
    public static int levenshteinDistance(String str1, String str2) {
        int[][] calc = new int[str2.length()+1][str1.length()+1];
        calc[0][0] = 0;
        for(int i=1;i<calc[0].length;i++){
            calc[0][i] = calc[0][i-1]+1;
        }
        for(int i=1;i<calc.length;i++){
            calc[i][0] = calc[i-1][0]+1;
        }
        for(int i=1;i<calc.length;i++){
            for(int j=1;j<calc[0].length;j++){
                int minData = Math.min(calc[i-1][j-1], Math.min(calc[i-1][j], calc[i][j-1]));
                calc[i][j] = (str2.charAt(i-1)==str1.charAt(j-1))?calc[i-1][j-1]:minData+1;
            }
        }
        return calc[str2.length()][str1.length()];
    }

    public static void print(int[][] arr){
        for (int[] ints : arr) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(ints[j] + " ");
            }
            System.out.println();
        }
        System.out.println("***************************************************");
    }

    public static void main(String[] args) {
        System.out.println(levenshteinDistance("abc", "yabd"));
    }
}
