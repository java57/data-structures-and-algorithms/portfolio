package codewithcrazy.companyprep.medium.dp.traversegraph;

import java.util.Arrays;

public class WaysToTraverseGraph {

    public static int numberOfWaysToTraverseGraph(int width, int height) {
        // Write your code here.
        int[][] ways = new int[height][width];
        for(int i=0;i<ways.length;i++){
            ways[i][0] = 1;
        }
        Arrays.fill(ways[0], 1);
        for(int i=1;i<ways.length;i++){
            for(int j=1;j<ways[0].length;j++){
                ways[i][j] = ways[i-1][j]+ways[i][j-1];
            }
        }
        return ways[height-1][width-1];
    }

    public static void main(String[] args) {
        System.out.println(numberOfWaysToTraverseGraph(2, 3));
    }
}
