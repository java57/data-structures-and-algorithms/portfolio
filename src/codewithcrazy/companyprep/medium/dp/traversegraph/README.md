**Problem**

```
Given two integers representing width and height of a 2-d space.
Calculate number of ways to reach till end 
```

**Example**
```
Input:
width: 2
height: 3

Output:
3

```

**Explanation**
```
 _ _
|_|_|
|_|_|
|_|_|

There are three ways to reach to bottom.
1. Right, Down, Down
2. Down, Right, Down
3. Down, Down, Right
```