**Problem**

```
Given an array of positive integers, return maximum sum of non-adjacent elements of array 
```

**Example**
```
Input:
[75, 105, 120, 75, 90, 135]

Output:
330

```

**Explanation**
```
330 = 75+120+135
```