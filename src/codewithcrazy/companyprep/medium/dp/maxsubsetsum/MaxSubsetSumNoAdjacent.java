package codewithcrazy.companyprep.medium.dp.maxsubsetsum;

public class MaxSubsetSumNoAdjacent {

    public static int maxSubsetSumNoAdjacent(int[] array) {
        // Write your code here.
        if(array.length == 0){
            return 0;
        }
        if(array.length == 1){
            return array[0];
        }
        if(array.length == 2){
            return Math.max(array[0], array[1]);
        }
        array[1] = Math.max(array[0], array[1]);
        for(int i=2; i<array.length;i++){
            array[i] = Math.max(array[i-1], array[i-2]+array[i]);
        }
        return array[array.length-1];
    }

    public static void main(String[] args) {
        System.out.println(maxSubsetSumNoAdjacent(new int[]{75, 105, 120, 75, 90, 135}));
    }
}
