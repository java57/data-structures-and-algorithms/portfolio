package codewithcrazy.companyprep.medium.dp.makechange;

import java.util.Arrays;

public class WaysToMakeChange {

    public static int numberOfWaysToMakeChange(int n, int[] denoms) {
        // Write your code here.
        int[] waysArr = new int[n+1];
        waysArr[0] = 1;
        Arrays.sort(denoms);
        for(int den:denoms){
            for(int i=1;i<waysArr.length;i++){
                if(den<=i){
                    waysArr[i] += waysArr[i-den];
                }
            }
        }
        return waysArr[n];
    }

    public static void main(String[] args) {
        System.out.println(numberOfWaysToMakeChange(6, new int[]{1, 5}));
    }
}
