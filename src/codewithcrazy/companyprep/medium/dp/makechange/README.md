**Problem**

```
Given an array of denominations and a target amount, calculate number of ways to generate the target amount from the denominations 
```

**Example**
```
Input:
n=6
denoms = [1, 5]

Output:
2

```

**Explanation**
```
We can get 6 using 2 combinations, 1*6 and 5*1 + 1*1
```