package codewithcrazy.companyprep.easy.trees.bfs;

import java.util.ArrayDeque;
import java.util.Deque;

public class BFS {

    private static Deque<TreeNode> treeQueue;

    public static void bfs(TreeNode root){
        TreeNode temp;
        treeQueue = new ArrayDeque<>();
        treeQueue.offerLast(root);

        while (!treeQueue.isEmpty()){
            temp = treeQueue.poll();
            System.out.print(temp.val+" ");
            if(temp.left!=null){
                treeQueue.offerLast(temp.left);
            }
            if(temp.right!=null){
                treeQueue.offerLast(temp.right);
            }
        }
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);

        bfs(root);
    }

    static class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
