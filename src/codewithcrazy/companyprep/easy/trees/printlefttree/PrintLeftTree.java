package codewithcrazy.companyprep.easy.trees.printlefttree;

public class PrintLeftTree {

    private static int currentLevel;

    public static void printLeftTree(TreeNode root){
        currentLevel = 0;
        printLeftTree(root, 1);
    }

    public static void printLeftTree(TreeNode node, int level){
        if(node == null){
            return;
        }
        if(level>currentLevel){
            currentLevel = level;
            System.out.println(node.val);
        }
        printLeftTree(node.left, level+1);
        printLeftTree(node.right, level+1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.left.left.right = new TreeNode(8);
        printLeftTree(root);
    }

    static class TreeNode{
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }

        public TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}
