**Problem**

```
Given a binary tree, print it's left view
```

**Example**
```
Input: 
                1
               / \
              /   \
             2     3
            / \   / \
           4   5 6   7

Output: 1, 2, 4
```