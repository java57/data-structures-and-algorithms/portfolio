package codewithcrazy.companyprep.easy.trees.nodedepth;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class NodeDepth {

    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        root.left = new BinaryTree(2);
        root.left.left = new BinaryTree(4);
        root.left.right = new BinaryTree(5);
        root.left.left.left = new BinaryTree(8);
        root.left.left.right = new BinaryTree(9);
        root.right = new BinaryTree(3);
        root.right.left = new BinaryTree(6);
        root.right.right = new BinaryTree(7);
        System.out.println(nodeDepths(root));
    }

    public static int nodeDepths(BinaryTree root) {
        // Write your code here.
        int totalDepth = 0;
        Deque<Level> levelDeque = new ArrayDeque<>();
        levelDeque.offerLast(new Level(root, 0));
        while(!levelDeque.isEmpty()){
            Level currLevel = levelDeque.poll();
            int depth = currLevel.depth;
            totalDepth+=depth;
            if(currLevel.node.left!=null){
                levelDeque.offerLast(new Level(currLevel.node.left, depth+1));
            }
            if(currLevel.node.right!=null){
                levelDeque.offerLast(new Level(currLevel.node.right, depth+1));
            }
        }
        return totalDepth;
    }

    static class Level{
        BinaryTree node;
        int depth;

        public Level(BinaryTree node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }

    static class BinaryTree {
        int value;
        BinaryTree left;
        BinaryTree right;

        public BinaryTree(int value) {
            this.value = value;
            left = null;
            right = null;
        }
    }
}
