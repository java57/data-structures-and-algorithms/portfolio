**Problem**

```
Given a binary tree, return a sum of all nodes' depths.
Node depth is the distance from root of the tree.
```

**Example**
```
Input: 
                1
               / \
              /   \
             2     3
            / \   / \
           4   5 6   7
          / \
         8   9

Output: 16

Explanation:
Depth of node 2: 1
Depth of node 3: 1
Depth of node 4: 2
Depth of node 5: 2
Depth of node 8: 3
and so on

Their sum would be: 16
```