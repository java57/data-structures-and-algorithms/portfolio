**Problem**

```
Given a binary tree, return a list of sum of all the branches.
```

**Example**
```
Input: 
                1
               / \
              /   \
             2     3
            / \   / \
           4   5 6   7

Output: [7, 8, 10, 11]

Explanation:
1+2+4 = 7
1+2+5 = 8
1+3+6 = 10
1+3+7 = 11
```