package codewithcrazy.companyprep.easy.trees.branchsum;

import java.util.ArrayList;
import java.util.List;

public class BranchSum {

    public static List<Integer> branchSums(BinaryTree root) {
        // Write your code here.
        List<Integer> output = new ArrayList<>();
        checkSum(0, root, output);
        return output;
    }

    public static void checkSum(int currentSum, BinaryTree node, List<Integer> output){
        if(node == null){
            return;
        }
        currentSum += node.value;
        checkSum(currentSum, node.left, output);
        checkSum(currentSum, node.right, output);
        if(node.left == null && node.right == null){
            output.add(currentSum);
        }
    }

    static class BinaryTree {
        int value;
        BinaryTree left;
        BinaryTree right;

        BinaryTree(int value) {
            this.value = value;
            this.left = null;
            this.right = null;
        }
    }

    public static void main(String[] args) {
        BinaryTree root = new BinaryTree(1);
        root.left = new BinaryTree(2);
        root.right = new BinaryTree(3);
        root.left.left = new BinaryTree(4);
        root.left.right = new BinaryTree(5);
        root.right.left = new BinaryTree(6);
        root.right.right = new BinaryTree(7);
        root.left.left.left = new BinaryTree(8);
        root.left.left.right = new BinaryTree(9);
        root.left.right.left = new BinaryTree(10);

        List<Integer> branchSum = branchSums(root);
        System.out.println(branchSum);
    }
}
