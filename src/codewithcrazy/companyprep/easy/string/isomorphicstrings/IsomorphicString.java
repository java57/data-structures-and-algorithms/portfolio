package codewithcrazy.companyprep.easy.string.isomorphicstrings;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IsomorphicString {

    public static boolean isIsomorphic(String s, String t) {
        Map<Character, Character> stringsCheck = new HashMap<>();
        Set<Character> checkedChar = new HashSet<>();
        for(int i=0;i<s.length();i++){
            if(stringsCheck.containsKey(s.charAt(i)) && stringsCheck.get(s.charAt(i))!=t.charAt(i)){
                return false;
            }
            if(!stringsCheck.containsKey(s.charAt(i))){
                if(checkedChar.contains(t.charAt(i))){
                    return false;
                }
                stringsCheck.put(s.charAt(i), t.charAt(i));
                checkedChar.add(t.charAt(i));
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isIsomorphic("egg", "add"));
        System.out.println(isIsomorphic("foo", "bar"));
        System.out.println(isIsomorphic("badc", "baba"));
        System.out.println(isIsomorphic("baba", "badc"));
    }

}
