**Problem**

```
Consider you've 2 strings. You need to find if you can get second string if you replace each character with another.
```

**Example**
```
Input: "egg", "add"
Output: true
Explanation: In this, 'e' can be replaced with 'a' and 'g' can be replaced with 'd'.
```

```
Input: "foo", "bar"
Output: false
Explanation: In this, 'f' can be replaced with 'b'.
             First 'o' can be replaced with 'a' however for second 'o' it would fail.
```