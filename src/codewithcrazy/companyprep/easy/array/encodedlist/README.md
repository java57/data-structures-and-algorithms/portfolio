**Problem**

```
Given an array representing encoded integers. You need to decode it.
Consider pair of integers, where first integer represents number of occurence and second represents actual number.
```

**Example**
```
Input: [1, 2, 3, 4]
Output: [2, 4, 4, 4]
Explanation: Break input in 2 parts, [1,2] and [3,4]. 
             You can read it as 1 occurence of 2 and 3 occurence of 4.
```