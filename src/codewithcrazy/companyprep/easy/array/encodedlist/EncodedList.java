package codewithcrazy.companyprep.easy.array.encodedlist;

import java.util.Arrays;

public class EncodedList {
    public static void main(String[] args) {
        int[] input = {1, 2, 3, 4};
        System.out.println(Arrays.toString(decompressList(input)));
        input = new int[]{1, 1, 2, 3};
        System.out.println(Arrays.toString(decompressList(input)));
    }

    public static int[] decompressList(int[] num){
        int newArr = 0;
        for(int i=0;i<num.length;i+=2){
            newArr+=num[i];
        }
        int[] output = new int[newArr];
        int idx = 0;
        for(int i=0;i<num.length;i+=2){
            while(num[i]>0){
                output[idx] = num[i+1];
                idx+=1;
                num[i]-=1;
            }
        }
        return output;
    }
}
