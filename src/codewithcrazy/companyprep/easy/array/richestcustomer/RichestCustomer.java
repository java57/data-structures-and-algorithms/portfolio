package codewithcrazy.companyprep.easy.array.richestcustomer;

public class RichestCustomer {
    public static int maximumWealth(int[][] accounts){
        int maxWealth = Integer.MIN_VALUE, tempSum;
        for (int[] account : accounts) {
            tempSum = 0;
            for (int i : account) {
                tempSum += i;
            }
            maxWealth = Integer.max(maxWealth, tempSum);
        }
        return maxWealth;
    }

    public static void main(String[] args) {
        System.out.println(maximumWealth(new int[][]{{1, 2, 3},{3, 2, 1}}));
    }
}
