**Problem**

```
You've an m*n matrix where array[i][j] is the wealth customer i has in j bank.
Find customer with max wealth and return wealth
```

**Example**
```
Input: [[1, 2, 3], [4, 3, 2]]
Output: 9
Explanation: Customer 0 has 1+2+3=6 however Customer 1 has 4+3+2=9
```