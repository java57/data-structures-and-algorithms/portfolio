package codewithcrazy.companyprep.easy.array.jewelsandstones;

public class JewelsAndStones1 {

    public static int numJewelsInStones(String jewels, String stones){
        int output = 0;
        for(Character c:stones.toCharArray()){
            if(jewels.indexOf(c)!=-1){
                output+=1;
            }
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println(numJewelsInStones("aA", "aAAbbbb"));
    }
}
