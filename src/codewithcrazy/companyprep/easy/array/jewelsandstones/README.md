**Problem**

```
Consider you have two strings representing jewels and stones, we need to identify how many stones are there for each jewel.
Jewel string contains unique characters and are case sensitive
```

**Example**
```
Input: jewels='aA', stones='aAABBB'
Output: 3
Explanation: There are 1 stone as 'a' and 2 stones as 'A'
```
```
Input: jewels='z', stones='ZZZZZ'
Output: 0
```