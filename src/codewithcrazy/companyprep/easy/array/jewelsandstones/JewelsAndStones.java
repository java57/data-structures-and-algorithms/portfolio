package codewithcrazy.companyprep.easy.array.jewelsandstones;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class JewelsAndStones {
    public static int numJewelsInStones(String jewels, String stones){
        Set<Character> jewelsMap = new HashSet<>();
        int output = 0;
        for(Character c:jewels.toCharArray()){
            jewelsMap.add(c);
        }
        for(Character c:stones.toCharArray()){
            if(jewelsMap.contains(c)){
                output+=1;
            }
        }
        return output;
    }
    public static void main(String[] args) {
        System.out.println(numJewelsInStones("aA", "aAAbbbb"));
    }
}
