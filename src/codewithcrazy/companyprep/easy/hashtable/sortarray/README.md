**Problem**

```
Given an array of integers. Sort the array with increasing order of numbers' frequency.
```

**Example**
```
Input: [1, 1, 2, 2, 2, 3]
Output: [3, 1, 1, 2, 2, 2]
```

```
Input: [-1, 1, -6, 4, 5, -6, 1, 4, 1]
Output: [5, -1, 4, 4, -6, -6, 1, 1, 1]
```