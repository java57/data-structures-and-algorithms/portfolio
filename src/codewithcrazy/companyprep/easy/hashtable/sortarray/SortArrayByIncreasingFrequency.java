package codewithcrazy.companyprep.easy.hashtable.sortarray;

import java.util.*;

public class SortArrayByIncreasingFrequency {

    public static int[] frequencySort(int[] nums) {
        Map<Integer, Integer> occurence = new HashMap<>();
        for (int num : nums) {
            occurence.put(num, (occurence.containsKey(num)?occurence.get(num)+1:1));
        }
        PriorityQueue<NumFreq> pq = new PriorityQueue<>();
        occurence.forEach((k,v)->{
            pq.add(new NumFreq(k, v));
        });
        int j=0;
        while (!pq.isEmpty()){
            NumFreq tempData = pq.poll();
            int i = tempData.freq;
            int data = tempData.num;
            while (i!=0){
                nums[j] = data;
                i-=1;
                j+=1;
            }
        }
        return nums;
    }
    static class NumFreq implements Comparable<NumFreq>{
        private int num;
        private int freq;

        public NumFreq(int num, int freq) {
            this.num = num;
            this.freq = freq;
        }

        @Override
        public int compareTo(NumFreq o) {
            if(freq==o.freq){
                return o.num-num;
            }
            return freq-o.freq;
        }
    }

    public static void main(String[] args) {
        int[] input = {1, 1, 2, 2, 2, 3};
        frequencySort(input);
        for(int i:input){
            System.out.print(i+" ");
        }
    }
}
