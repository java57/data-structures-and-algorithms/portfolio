**Problem**

```
Consider 2 strings, find the longest subsequence from them.
```

**Example**
```
Input:
str1: ZXVVYZW
str2: XKYKZPW

Output: ["X", "Y", "Z", "W"]
```