package codewithcrazy.companyprep.hard.dp.longestcommonsubs;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class LongestCommonSubsequence {

    public static List<Character> longestCommonSubsequence(String str1, String str2) {
        // Write your code here.
        if(str1.isEmpty() || str2.isEmpty()){
            return null;
        }
        int[][] arrCheck = new int[str2.length()+1][str1.length()+1];
        for(int i=1;i<arrCheck.length;i++){
            for(int j=1;j<arrCheck[0].length;j++){
                if(str1.charAt(j-1) == str2.charAt(i-1)){
                    arrCheck[i][j] = arrCheck[i-1][j-1]+1;
                }
                else {
                    arrCheck[i][j] = Math.max(arrCheck[i-1][j], arrCheck[i][j-1]);
                }
            }
        }
        if(arrCheck[str2.length()][str1.length()]==0){
            return null;
        }
        return getOutput(arrCheck, str1);
    }
    public static List<Character> getOutput(int[][] array, String str1){
        List<Character> output = new ArrayList<>();
        Stack<Character> outputStack = new Stack<>();
        int i = array.length-1, j = array[0].length-1;
        while(i!=0 && j!=0){
            if(array[i-1][j] == array[i][j]){
                i-=1;
            }
            else if(array[i][j-1] == array[i][j]){
                j-=1;
            }
            else{
                outputStack.add(str1.charAt(j-1));
                i-=1;
                j-=1;
            }
        }
        while (!outputStack.isEmpty()){
            output.add(outputStack.pop());
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println(longestCommonSubsequence("ZXVVYZW", "XKYKZPW"));
    }
}
