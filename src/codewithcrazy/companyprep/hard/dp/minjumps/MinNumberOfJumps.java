package codewithcrazy.companyprep.hard.dp.minjumps;

public class MinNumberOfJumps {
    public static int minNumberOfJumps(int[] array) {
        // Write your code here.
        if(array.length == 1){
            return 0;
        }
        int output = 0;
        int maxReach = array[0], steps = array[0] ;
        for(int i=1;i<array.length-1;i++){
            maxReach = Math.max(maxReach, array[i]+i);
            steps-=1;
            if(steps == 0){
                output+=1;
                steps = maxReach-i;
            }
        }

        return output+1;
    }

    public static void main(String[] args) {
        System.out.println(minNumberOfJumps(new int[]{3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3}));
    }
}
