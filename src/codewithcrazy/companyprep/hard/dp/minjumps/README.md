**Problem**

```
Consider 1d integer array, where each integer represents number of steps you can take.
Find minimum number of steps you can take to reach last node.
```

**Example**
```
Input:
[3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3]

Output: 4

Explanation: from 3->4->3->7->3
```