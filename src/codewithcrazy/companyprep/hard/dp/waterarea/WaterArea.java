package codewithcrazy.companyprep.hard.dp.waterarea;

public class WaterArea {
    public static int waterArea(int[] heights) {
        // Write your code here.
        int[] max = new int[heights.length];
        int output = 0;
        int leftMax = 0;
        for(int i=0;i<heights.length;i++){
            max[i] = leftMax;
            leftMax = Math.max(leftMax, heights[i]);
        }
        int rightMax = 0;
        for(int i=heights.length-1;i>=0;i--){
            int minHeight = Math.min(rightMax, max[i]);
            if(minHeight<heights[i]){
                max[i] = 0;
            }
            else {
                max[i] = minHeight-heights[i];
            }
            rightMax = Math.max(rightMax, heights[i]);
        }
        for (int j : max) {
            output += j;
        }
        return output;
    }

    public static void main(String[] args) {
        System.out.println(waterArea(new int[]{0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3}));
    }

}