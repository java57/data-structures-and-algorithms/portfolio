**Problem**

```
Given a non-negative 1d array, where each non zero number represent height of wall with width as 1, calculate max area that array can store.
```

**Example**
```
Input:
[0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3]

Output: 48

Explanation: Below is the array of water captured at each node
[0, 0, 8, 8, 3, 8, 8, 0, 3, 3, 2, 2, 3, 0]
```