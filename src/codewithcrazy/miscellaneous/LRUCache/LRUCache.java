package codewithcrazy.miscellaneous.LRUCache;

import codewithcrazy.miscellaneous.LRUCache.models.UserProfile;

import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache {

    public static void main(String[] args) throws InterruptedException {
        Cache cache = new Cache();
        UserProfile userProfile;
        System.out.println("Aditya's profile: ");
        System.out.println(cache.getUserProfile("aditya"));
        cache.printCache();

        System.out.println("Aniruddh's profile: ");
        System.out.println(cache.getUserProfile("aniruddh"));
        cache.printCache();

        System.out.println("Aditya's profile: ");
        System.out.println(cache.getUserProfile("aditya"));
        cache.printCache();

        System.out.println("Manish's profile: ");
        System.out.println(cache.getUserProfile("badshah"));
        cache.printCache();

        System.out.println("Ketul's profile: ");
        System.out.println(cache.getUserProfile("ketul"));
        cache.printCache();

        System.out.println("Manish's profile: ");
        System.out.println(cache.getUserProfile("badshah"));
        cache.printCache();

        System.out.println("Spandan's profile: ");
        System.out.println(cache.getUserProfile("spandan"));
        cache.printCache();

    }

}

class Cache{
    private Map<String, Node> userProfileCache;
    private UserProfileWarehouse userProfileWarehouse;
    private Node head, tail;
    private static final Integer CACHE_SIZE = 3;
    public Cache(){
        userProfileWarehouse = new UserProfileWarehouse();
        head = null;
        tail = null;
    }

    public UserProfile getUserProfile(String userId) throws InterruptedException {
        UserProfile response;
        if(userProfileCache == null){
            userProfileCache = new LinkedHashMap<>();
        }
        if(!userProfileCache.containsKey(userId)){
            System.out.println("Getting details from warehouse");
            UserProfile userProfile = userProfileWarehouse.getUserProfile(userId);
            if(userProfileCache.size() == CACHE_SIZE){
                removeDataFromTail();
            }
            addDataToCache(userProfile);
            response = userProfile;
        }
        else{
            System.out.println("Getting data from cache");
            Node data = userProfileCache.get(userId);
            repositionData(data);
            response = (UserProfile) data.getCachedData();
        }

        return response;
    }

    public void repositionData(Node data){
        if(head != data){
            if(tail == data){
                tail = data.prev;
            }
            if(data.next != null){
                data.next.prev = data.prev;
            }
            data.prev.next = data.next;
            data.prev = null;
            data.next = head;
            head.prev = data;
            head = data;
        }

    }

    public void addDataToCache(UserProfile userProfile){

        Node newNode = new Node(userProfile);
        userProfileCache.put(userProfile.getUserId(), newNode);
        if(userProfileCache.size() == 1){
            newNode.prev = null;
            head = newNode;
            tail = newNode;
            return;
        }
        newNode.next = head;
        newNode.prev = null;
        head.prev = newNode;
        head = newNode;
    }

    private void removeDataFromTail(){
        Node temp = tail;
        tail = tail.prev;
        tail.next = null;
        UserProfile userProfile = (UserProfile) temp.getCachedData();
        userProfileCache.remove(userProfile.getUserId());
    }

    public void printCache(){
        System.out.println("-----------------------------------");
        System.out.println("Data in cache: ");
        if(head != null){
            Node temp = head;
            System.out.print("null - ");
            while(temp != null){
                UserProfile userProfile = (UserProfile) temp.getCachedData();
                System.out.print(userProfile.getUserId()+" - ");
                temp = temp.next;
            }
            System.out.println("null");
        }
        System.out.println("-----------------------------------\n");
    }

    static class Node{
        Object cachedData;
        Node prev;
        Node next;
        Node(Object data){
            cachedData = data;
            prev = null;
            next = null;
        }

        public Object getCachedData() {
            return cachedData;
        }
    }
}