package codewithcrazy.miscellaneous.LRUCache;

import codewithcrazy.miscellaneous.LRUCache.models.UserProfile;

import java.util.ArrayList;
import java.util.List;

public class UserProfileWarehouse {
    List<UserProfile> userProfiles;

    public List<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(List<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    public UserProfile getUserProfile(String userId) throws InterruptedException {
        Thread.sleep(2000);
        if(userProfiles==null || userProfiles.isEmpty()){
            populateDummyData();
        }
        return userProfiles.stream().filter(up->up.getUserId().equals(userId)).findFirst().get();
    }

    private void populateDummyData(){
        userProfiles = new ArrayList<>();
        userProfiles.add(new UserProfile("aditya", 32, "married"));
        userProfiles.add(new UserProfile("aniruddh", 34, "married"));
        userProfiles.add(new UserProfile("spandan", 33, "single"));
        userProfiles.add(new UserProfile("badshah", 32, "married"));
        userProfiles.add(new UserProfile("ketul", 35, "married"));
    }
}
