**LRU Cache**

```
Purpose of this cache is to remove less recently used data from cache whenever it's full.
```


**Solution**
```
Description
---------------------------
In the used approach, I've tried to put user profile in cache and return it if it's present.
In case it's not there in cache, it would pick the data from warehouse and wait for 2 seconds before adding in cache and returning data.


Example used
---------------------------
Cache size used: 3

1. Aditya's profile fetched
Cache: head - Aditya - tail

2. Aniruddh's profile fetched
Cache: head - Aniruddh - Aditya - tail

3. Aditya's profile fetched
Cache: head - Aditya - Aniruddh - tail

4. Manish's profile fetched
Cache: head - Manish - Aditya - Aniruddh - tail

5. Ketul's profile fetched
Cache: head - Ketul - Manish - Aditya - tail

6. Manish's profile fetched
Cache: head - Manish - Ketul - Aditya - tail

7. Spandan's profile fetched
Cache: head - Spandan - Manish - Ketul - tail
```
