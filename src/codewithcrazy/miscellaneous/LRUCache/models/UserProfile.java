package codewithcrazy.miscellaneous.LRUCache.models;

public class UserProfile {
    private String userId;
    private Integer age;
    private String maritalStatus;

    public UserProfile(String userId, Integer age, String maritalStatus) {
        this.userId = userId;
        this.age = age;
        this.maritalStatus = maritalStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    @Override
    public String toString() {
        return "(" +
                "userId='" + userId + '\'' +
                ", age=" + age +
                ", maritalStatus='" + maritalStatus + '\'' +
                ')';
    }
}
